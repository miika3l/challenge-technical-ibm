def PROJECT = 'challenge-technical-ibm'

def REPO_URL = 'https://gitlab.com/miika3l/'

def NEXUS_URL = env.NEXUS_REPO_URL

def GROUP_ID = 'com/ibm/challenge/'

def DOCKER_REGISTRY = env.DOCKER_REGISTRY_ADDRESS

def IMAGE_TAG = DOCKER_REGISTRY + '/' + PROJECT + ':$VERSION.' + env.BUILD_NUMBER

pipeline {
    agent any
    stages {

        stage ('Checkout Code Stage') {
            steps {
                echo "Start Checkout Code Stage"
				dir(PROJECT) {
					checkout([$class: 'GitSCM', branches: [[name: '*/${BRANCH_NAME}']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab_login', url:REPO_URL+PROJECT+'.git']]])
				}
                echo "End Checkout Code Stage"
            }
        }

        stage ('Compile Stage') {
	        steps {
                echo "Start Compile Stage"
				dir(PROJECT) {
					withMaven(maven : 'MAVEN_3_5_0') {
						sh 'mvn -U clean compile'
					}
				}
                echo "End Compile Stage"
		    }
	    }

	    stage ('Testing Stage') {
	        steps {
                echo "Start Testing Stage"
				dir(PROJECT) {
					withMaven(maven : 'MAVEN_3_5_0') {
						sh 'mvn test -DskipTests=true'
					}
				}
                echo "End Testing Stage"
		    }
	    }

	    stage ('Package Stage') {
	        steps {
                echo "Start Package Stage"
				dir(PROJECT) {
					withMaven(maven : 'MAVEN_3_5_0') {
						echo "ArtifactId: " + readMavenPom().getArtifactId()
						sh 'mvn -U clean package -DskipTests=false'
					}
				}
                echo "End Package Stage"
		    }
	    }

	    stage ('Upload Artifact Stage') {
	        steps {
				echo "Start Upload Artifact Stage"
				dir(PROJECT) {
					withCredentials([usernamePassword(credentialsId: 'nexus_login', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
						configFileProvider(
							[configFile(fileId: '404b4168-683d-46af-8dff-bc91ab2ce9a7', variable: 'UPLOAD_ARTIFACT_TO_NEXUS_SHELL')]) {
								sh 'chmod 755 $UPLOAD_ARTIFACT_TO_NEXUS_SHELL'
								sh "$UPLOAD_ARTIFACT_TO_NEXUS_SHELL $USERNAME $PASSWORD " + env.NEXUS_REPO_URL + " ${GROUP_ID} '" + readMavenPom().getArtifactId()  + "' ${VERSION} '" + readMavenPom().getPackaging() + "'"
						}
					}
				}
                echo "End Upload Artifact Stage"
		    }
	    }

        stage ('Download Artifact Stage') {
            steps {
                echo "Start Download Artifact Stage"
				dir(PROJECT) {
					sh 'rm ' + PROJECT + '-$VERSION.jar || true'
					configFileProvider(
						[configFile(fileId: '1417b19b-98d5-4109-8e6f-67702709aca4', variable: 'DOWNLOAD_ARTIFACT_FROM_NEXUS_SHELL')]) {
							sh 'chmod 755 $DOWNLOAD_ARTIFACT_FROM_NEXUS_SHELL'
							sh "$DOWNLOAD_ARTIFACT_FROM_NEXUS_SHELL " + env.NEXUS_REPO_URL + " ${GROUP_ID} '" + PROJECT  + "' ${VERSION} 'jar'"
					}
				}
                echo "End Download Artifact Stage"
            }
        }

	    stage ('Publish Docker Image Stage') {
	        steps {
                echo "Start Publish Docker Image Stage"
				dir(PROJECT) {
					echo "=> Preparing Dockerfile for ${PROJECT}..."
					
					sh 'cp src/main/resources/Dockerfile .'
					
					echo "Building Docker Image for ${PROJECT}..."
					sh 'sudo docker build -t ' + IMAGE_TAG + ' --build-arg TARGET_FILE=' + PROJECT + '-$VERSION.jar .'

					echo "Publishing Docker Image for ${PROJECT}..."
					
					withCredentials([usernamePassword(credentialsId: 'registry_login', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
					    sh 'sudo docker login -u $USERNAME -p $PASSWORD ' + DOCKER_REGISTRY
					}

					sh 'sudo docker push ' + IMAGE_TAG
					
					sh 'sudo docker logout ' + DOCKER_REGISTRY
				}
                echo "End Publish Docker Image Stage"
		    }
	    }
	    
	    stage ('Start Docker Container Stage') {
	        steps {
                echo "Start Docker Container Stage"
				dir(PROJECT) {
					sh 'echo VERSION=' + VERSION + '.' + env.BUILD_NUMBER + ' >> .env'
					sh 'cat .env'
					sh 'sudo docker-compose rm -f -s -v'
					sh 'sudo docker-compose up -d'
				}
                echo "End Start Docker Container Stage"
		    }
	    }

    }
    
	post {
	    always {	    	
		    cleanWs()
		    notifySlack(currentBuild.currentResult)
	    }
    }
}

@NonCPS
def notifySlack(String buildStatus = 'STARTED') {
    def jobName = "${env.JOB_NAME}"
    def buildNumber = "${env.BUILD_NUMBER}"
    def buildURL = "${env.BUILD_URL}"
    def buildUser = getBuildUser()
    
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESS'
    

    // Default values
    def colorCode = '#FF0000'
    def subject = 'STATUS: ' + buildStatus + ": <${env.BUILD_URL}|Execution #${env.BUILD_NUMBER}> of job ${env.JOB_NAME}"
    
    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESS') {
        colorCode = '#33FF96'
    } else {
        colorCode = '#A52A2A'
    }
    
    def attachments = [
	  [
	    text: subject,
	    fallback: 'Hey, Vader seems to be mad at you.',
	    color: colorCode
	  ]
	]
    
    slackSend(channel: '#challenge-devops', attachments: attachments)
}

@NonCPS
def getBuildUser() {
        return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
}