package com.ibm.challenge.suma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengeTechnicalIbmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeTechnicalIbmApplication.class, args);
	}

}
