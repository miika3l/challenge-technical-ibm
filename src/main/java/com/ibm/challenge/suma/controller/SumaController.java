package com.ibm.challenge.suma.controller;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibm.challenge.suma.entities.Suma;
import com.ibm.challenge.suma.services.MathService;

@Controller
@EnableAutoConfiguration
public class SumaController {
	
	private static final Logger logger = LogManager.getLogger(SumaController.class);
	
	@Autowired
	MathService mathService;
	
	
	@RequestMapping(value = "/sumar/{a}/{b}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> sumar(@PathVariable("a") String a, @PathVariable("b") String b) {
		
		logger.info("REQUEST");
		
		Suma suma = new Suma();
		suma.setSumando01(new Double(a));
		suma.setSumando02(new Double(b));				
		
		Map<String, Object> mapResponse = mathService.register(suma);
		
		logger.debug("Resultado: " + suma);
		
		return mapResponse;
	}
	
	@RequestMapping(value = "/listar", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> listar() {
		Map<String, Object> mapResponse = mathService.findAll();
		
		return mapResponse;
	}

}
