package com.ibm.challenge.suma.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.challenge.suma.entities.Suma;
import com.ibm.challenge.suma.repositories.SumaRepository;

@Service
@Transactional
public class MathService {

	@Autowired
	private SumaRepository sumaRepository;
	
	public Map<String, Object> findAll() {
		String status = "SUCCESS";
		String message = "The transaction has been successfully executed";
		
		Map<String, Object> mapResponse = new HashMap<String, Object>();
		List<Suma> sumaList = sumaRepository.findAll();
		
		mapResponse.put("status", status);
		mapResponse.put("message", message);
		mapResponse.put("response", sumaList);

		
		return mapResponse;
	}
	
	public Map<String, Object> register(Suma entity) {
		String status = "SUCCESS";
		String message = "The transaction has been successfully executed";
		Map<String, Object> mapResponse = new HashMap<String, Object>();
		
		Double resultado = entity.getSumando01() + entity.getSumando02();
		entity.setResultado(resultado);
		
		sumaRepository.register(entity);
		
		mapResponse.put("status", status);
		mapResponse.put("message", message);
		mapResponse.put("response", entity.getResultado());
		
		return mapResponse;
	}

}
