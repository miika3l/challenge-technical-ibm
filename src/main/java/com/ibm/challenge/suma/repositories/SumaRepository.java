package com.ibm.challenge.suma.repositories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ibm.challenge.suma.entities.Suma;

@Repository
public class SumaRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public List<Suma> findAll() {
		String sql = "SELECT * FROM suma";
		
		List<Suma> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Suma.class));
		
		return result;
	}
	
	public void register(Suma entity) {
		String sql = "INSERT INTO suma(sumando01, sumando02, resultado) VALUES(?,?,?)";
		
		jdbcTemplate.update(sql,
				entity.getSumando01(), 
				entity.getSumando02(),
				entity.getResultado());
	}
	
}
