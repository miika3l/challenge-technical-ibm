package com.ibm.challenge.suma.entities;

public class Suma {

	private Double sumando01;
	private Double sumando02;
	private Double resultado;
	
	public Double getSumando01() {
		return sumando01;
	}
	
	public void setSumando01(Double sumando01) {
		this.sumando01 = sumando01;
	}
	
	public Double getSumando02() {
		return sumando02;
	}
	
	public void setSumando02(Double sumando02) {
		this.sumando02 = sumando02;
	}

	public Double getResultado() {
		return this.resultado;
	}

	public void setResultado(Double resultado) {
		this.resultado = resultado;
	}

	@Override
	public String toString() {
		return String.format(
                "Suma [sumando01=%f, sumando02=%f, resultado=%f]",
                sumando01, 
                sumando02, 
                resultado);
	}
	
}
