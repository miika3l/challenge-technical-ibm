CREATE DATABASE /*!32312 IF NOT EXISTS*/`example` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;

USE `example`;

/*Table structure for table `suma` */

DROP TABLE IF EXISTS `suma`;

CREATE TABLE `suma` (
  `sumando01` DECIMAL(10,2),
  `sumando02` DECIMAL(10,2),
  `resultado` DECIMAL(10,2)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;