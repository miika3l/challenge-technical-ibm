### Installing

Run Jenkins Job
```
http://ec2-54-68-110-122.us-west-2.compute.amazonaws.com:8180/jenkins/job/DEPLOY_CHALLENGE_TECHNICAL_IBM/
```

## Try out

Sumar
```
http://ec2-54-68-110-122.us-west-2.compute.amazonaws.com:8080/api/sumar/90/90
```

Listar
```
http://ec2-54-68-110-122.us-west-2.compute.amazonaws.com:8080/api/listar
```

### Additional Information

Jenkins: admin/admin
```
http://ec2-54-68-110-122.us-west-2.compute.amazonaws.com:8180/jenkins
```

Nexus: admin/admin
```
http://ec2-54-68-110-122.us-west-2.compute.amazonaws.com:8085/nexus/
```

Portainer: admin/adminadmin
```
http://ec2-54-68-110-122.us-west-2.compute.amazonaws.com:9000/#/auth
```

Join Slack Group for Receive Notifications
```
https://join.slack.com/t/miikaelcorporationinc/shared_invite/enQtNzAxMjE0NTM3Mzk1LTJjYjk5NzVhMjQ1N2I5MzBlOTcxODNjMjcxNzUyMmQwNzg1ZjdhNjljOGJjNWVhNWNjNjIzMzljMzhjY2IzNjM
```